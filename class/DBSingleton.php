<?php

class DBSingleton {
	//require ("inc/auth.inc.php");
	protected static $instance;//con self se accede a variable static en la propia clase

	protected function __construct() {

	}

	public static function getInstance() {//funcion para crear conexion a db en otros archivos.
			$host = "localhost";
			$dbname = "sakila";
			$pass = "";
			$user = "root";
		if(empty(self::$instance)) {
			try {
				self::$instance = new PDO("mysql:host=".$host.";dbname=".$dbname, $user, $pass);
				self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
			} catch(PDOException $error) {
				echo $error->getMessage();
			}
		}
		return self::$instance;
	}
}	

?>