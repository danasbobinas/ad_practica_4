<?php 


?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
</head>
<body>
	<form method="POST">
		<label>Title: </label>
		<input type="text" name="nombre" id="title">
		<input type="button" name="enviar" value="Buscar" id="buscar">
		</br>
		<div id="resultado"></div>
		</br></br>
		<label>Insert movie: </label>
		</br>
		<label>Title: </label>
		<input type="text" name="title_film" id="title_film">
		<label>Description: </label>
		<input type="text" name="description" id="description">
		<label>Release year: </label>
		<input type="text" name="year" id="release_year">
		<label>Language: </label>
		<select id="language_id">
			<option>1</option>
			<option>2</option>
			<option>3</option>
		</select>
		<input type="button" name="insertar" value="Insertar" id="insertar">
		</br></br>
		<label>Delete Film</label>
		</br>
		<input type="text" id=film_id placeholder="Insert film_id">
		<input type="button" id="delete_film" value="borrar">
	
	</form>
		<div id="mensaje"></div>

	<script type="text/javascript">
		$(document).ready(function(){
			$("#delete_film").click(function(){
				$.ajax({
					method: "POST",
					url: "film.php",
					data: {
						accion: "borrar",
						film_id: $("#film_id").val()
					},
					dataType: "json"
				})
				.done(function(response){
					if(response.success){
						$("#mensaje").html(response.msg);
					}else{
						$("#mensaje").html(response.msg);
					}
				})
				.fail(function(){
					alert("error");
				});
			});
		});

	</script>

	<script type="text/javascript">
			$(document).ready(function(){
				$("#insertar").click(function(){
					$.ajax({
						method:"POST",
						url: "film.php",
						data: {
							accion: "insertar",
							title_film: $("#title_film").val(),
							description: $("#description").val(),
							release_year: $("#release_year").val(),
							language_id: $("#language_id").val()
						},
						dataType: "json"
					})
					.done(function(response){
						if(response.success){
							$("#mensaje").html(response.msg);
						}
					})
					.fail(function(){
						alert("error");
					});
				});
			});
	</script>
	

	<script type="text/javascript"> //script para mostrar datos(buscador)
		
		$ (document).ready(function(){//me aseguro que esta abierto el documento

			$("#buscar").click(function(){//cuando le haga click al button buscar ejecuta ajax
				
				$.ajax({//abro ajax
					method: "POST",
					url: "film.php",//ruta donde esta mi servicio web
					data: {//con el data le envio todo lo que hay dentro el
						accion : "consulta",//con esto identifico que hare con este servicio
						title : $("#title").val()//aqui le paso el nombre del film
					},
					dataType: "json"
				})

				.done(function( response ){//done comprueba si se recibe la respuesta del servicio web, con variable del metodo response(response la uso en el servicio web)
					if (response.success){//si no hay fallo
						$("#resultado").html(response.data);//en insertar muestro el msg de json convertido a html
						$("#mensaje").html(response.msg);
					}else{
						alert("error");
					}
				})
				.fail(function(){
					alert("error");
				});
			});
		});

	</script>


</body>
</html>