<?php
	require_once ("class/Film.php");//se necesita la clase para poder acceder a ella 

	
	$accion= isset($_POST["accion"])? $_POST["accion"] : null;
	$title= isset($_POST["title"])? $_POST["title"] : null;
	$title_film= isset($_POST["title_film"])? $_POST["title_film"] : null;
	$description= isset($_POST["description"])? $_POST["description"] : null;
	$release_year= isset($_POST["release_year"])? $_POST["release_year"] : null;
	$language_id= isset($_POST["language_id"])? $_POST["language_id"] : null;
	$film_id= isset($_POST["film_id"])? $_POST["film_id"] : null;//film_id for delete sql.

	$success=false;
	$response=null;

	switch ($accion) {
		case 'consulta':
			/*$json = json_encode(array(
				"success"=>true,
				"msg"=>"Listado de film.",
				"data"=>Film::getArrayFilm($title)//Film:: y funcion, para acceder a static functions de clases ajenas
				));*/
			
			$success=true;
			$response["msg"]="";
			$response["success"]= $success;
			$response["data"]=Film::getArrayFilm($title);//Film:: y funcion, para acceder a static functions de clases ajenas.
			break;

		case 'insertar':
			$film = new Film();
			$film->insertFilm($title_film, $description, $release_year, $language_id);
			$success=true;
			$response["msg"]="Film inserted";
			$response["success"]= $success;
			break;

		case 'borrar':
			$film = new Film();
				if($film->deleteFilm($film_id)){
					$success=true;
					$response["msg"]="Film deleted";
					$response["success"]= $success;
				}else{
					$success=true;
					$response["msg"]="Film not found";
					$response["success"]= $success;
				}
			break;
		default:
			
			break;
	}

	echo json_encode($response);
	//echo $json;

?>