<?php
	//require_once("Conexion.php");//require si hay fallo de conexion, no funcionara la clase, entonces uso include
	include_once("DBSingleton.php");//con include si hay fallo de conexion, muestra error pero el resto funciona

	class Film{

		private $title;
		private $description;
		private $release_year;
		private $language_id;
		private $original_language_id;
		private $rental_duration;
		private $rental_rate;
		private $length;
		private $replacement_cost;
		private $rating;
		private $special_features;
		private $image;
		private $last_update;

		public function __constructor($title, $description, $release_year, $language_id){
			$this->setTitle($title);
			$this->setDescription($description);
			$this->setRealease_year($release_year);
			$this->setLanguage_id($language_id);
			$this->setOriginal_language_id($original_language_id);
			$this->setRental_duration($rental_duration);
			$this->setRental_rate($rental_rate);
			$this->setLength($length);
			$this->setReplacement_cost($replacement_cost);
		}
		public static function deleteFilm($film_id){
			try{
				$conexion= DBSingleton::getInstance();//llamo al metodo getInstance de Conexion para crear una conexion.
				$sql= "Delete from film where film_id=".$film_id;
				$sen= $conexion->prepare($sql);
				//$sen->bindValue('film_id', $film_id);
				$sen->execute();
				$estado=false;
				if($filas=$sen->rowCount()>=1){
					$estado=true;
				}
				return $estado;
			}catch(Exception $e){
				echo $e->getMessage();
			}
		}
		public static function insertFilm($title, $description, $release_year, $language_id){
			try{
				$conexion= DBSingleton::getInstance();
				$sql= "Insert into film (title, description, release_year, language_id) values(:title, :description, :release_year, :language_id)";
				$sentencia= $conexion->prepare($sql);
				$sentencia->bindValue('title',$title);//con funcion bindValue le doy valor a los marcadores de la sql. 
				$sentencia->bindValue('description', $description);
				$sentencia->bindValue('release_year', $release_year);
				$sentencia->bindValue('language_id', $language_id);
				$sentencia->execute();
			}catch(Exception $e){
				echo $e->getMessage();
			}
		}

		public static function getArrayFilm($filters){
			try {
				$conexion = DBSingleton::getInstance();
				$sql = "Select title from film where title like '%".$filters."%'";
				$sentencia = $conexion->prepare($sql);
				$sentencia->execute();

				if($filas=$sentencia->rowCount()>=1){
					while ($resultado = $sentencia->fetch(PDO::FETCH_ASSOC)) {//fecth devuelve dato a dato y guardo en resultado, cuando fetch no devuelve fila(da error y asigna false asi para el while)
 					$films[]= $resultado;//en array films guardo cada fila devuelta por fetch.
					}
				}else{
					$films[]="";
				}
				/*for($i=0;$i<$resultado = $sentencia->fetch(PDO::FETCH_ASSOC);$i++){ Lo mismo que con el while.
					$films[$i]['title']=$resultado['title'];
				}*/
				return json_encode($films);//tengo que convertir el array php en array javascript(tipo json) para poder leerlo en el json y enviarle como json;
			} catch (Exception $e) {
				echo $e->getMessage();
  
			}
		}

		public function getTitle(){
		    return $this->title;
		}
		 
		public function setTitle($title){
		    $this->title = $title;
		}

		public function getDescription(){
		    return $this->description;
		}
		 
		public function setDescription($description){
		    $this->description = $description;
		}

		public function getRelease_year(){
		    return $this->release_year;
		}
		 
		public function setRelease_year($release_year){
		    $this->release_year = $release_year;
		}

		public function getLanguage_id(){
		    return $this->language_id;
		}
		 
		public function setLanguage_id($language_id){
		    $this->language_id = $language_id;
		}

		public function getOriginal_language_id(){
		    return $this->original_language_id;
		}
		 
		public function setOriginal_language_id($original_language_id){
		    $this->original_language_id = $original_language_id;
		}

		public function getRental_duration(){
		    return $this->rental_duration;
		}
		 
		public function setRental_duration($rental_duration){
		    $this->rental_duration = $rental_duration;
		}

		public function getRental_rate(){
		    return $this->rental_rate;
		}
		 
		public function setRental_rate($rental_rate){
		    $this->rental_rate = $rental_rate;
		}
		public function getLength(){
		    return $this->length;
		}
		 
		public function setLength($length){
		    $this->length = $length;
		}

		public function getReplacement_cost(){
		    return $this->replacement_cost;
		}
		 
		public function setReplacement_cost($replacement_cost){
		    $this->replacement_cost = $replacement_cost;
		}

	}

?>